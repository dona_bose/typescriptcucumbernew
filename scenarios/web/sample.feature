Feature: CreditBalance

@CucumberScenario
Scenario: Credit

  Given get "https://qas.qmetry.com/bank"
  #example of custom step
  And login with "Bob" and "Bob"
  #example of inbuilt step with locator repository
  And sendkeys "credit.input" into "1200"
  Then click on "credit.button"
  Then click on "checkbalence.button"
  #example of inbuilt step with direct locator
  And verify "id=user-globe-rank" is present
